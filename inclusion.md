# Political News
```
politics
geopolitics
CapitolConsequences (event-focused)
```

# Political Ideologies/Parties/Movements
```
Conservative
Libertarian
socialism
democrats
Anarchism
Republican
BreadTube
DemocraticSocialism
Anarcho_Capitalism
BlackLivesMatter
esist
neoliberal
liberal
```

# Political Figures
```
SandersForPresident
The_Mueller
OurPresident
AOC
Political_Revolution
YangForPresidentHQ
```

# Political Discussion
```
PoliticalDiscussion
NeutralPolitics
Keep_Track
moderatepolitics
```

# Political Banter
```
PoliticalHumor
ABoringDystopia
PoliticalCompassMemes
TrumpCriticizesTrump
anime_titties
ENLIGHTENEDCENTRISM
PresidentialRaceMemes
ShitLiberalsSay
libertarianmeme
```

# General News (excluded)
```
worldnews
news
```

# All (35 subreddits)
```
politics
PoliticalHumor
PoliticalDiscussion
Conservative
ABoringDystopia
SandersForPresident
Libertarian
PoliticalCompassMemes
NeutralPolitics
TrumpCriticizesTrump
socialism
geopolitics
anime_titties
democrats
Anarchism
Keep_Track
Republican
ENLIGHTENEDCENTRISM
The_Mueller
moderatepolitics
OurPresident
BreadTube
AOC
DemocraticSocialism
Anarcho_Capitalism
PresidentialRaceMemes
Political_Revolution
ShitLiberalsSay
libertarianmeme
CapitolConsequences
BlackLivesMatter
YangForPresidentHQ
esist
neoliberal
liberal
```

Added in `r/esist`, `r/neoliberal`, `r/liberal` above per `README.md` explanations.