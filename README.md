# Classifying Political Subreddits

## High-Level Goals

**Inclusion Principle:** Must be SFW (safe-for-work) and focused on U.S. or global political news, political ideologies/parties/movements, political figures, or political discussion.

**Threshold:** Must have more than 100,000 subscribers.

## Implementation

Scrap all subreddits with more than 100,000 subscribers from [FrontPageMetrics](https://frontpagemetrics.com/top-sfw-subreddits), ~3,000 subreddits, and keyword filter this set using their `public_descriptions` given from PRAW and manually separate the remaining subreddits.

## Files

- `define_corpus.ipynb` contains all the code to develop the final set of subreddits.

- `raw_subreddits.txt` contains the raw SFW subreddits on [FrontPageMetrics](https://frontpagemetrics.com/top-sfw-subreddits) with over 100,000 subscribers without `/r/` prefix.

- `subreddit_corpus.csv` contains the same subreddits in `raw_subreddits.txt` but with their respective `public_descriptions` pulled from PRAW.

- `keywords.txt` contains the keywords to filter the `public_descriptions`.

- `credentials.txt` contains the PRAW credentials to get access to the API.

- `filtered_corpus.csv` contains the subreddits after keyword filtering.

- `inclusion.txt` are the final subreddits to include based on `filtered_corpus.csv`.

## Exception Subreddits

- `r/esist` is included. It doesn't have a `public_description` to pass the filter. Meets subscriber threshold.

- `r/neoliberal` is included. It has a `public_description that is a joke and doesn't reflect the contents of the subreddit. Meets subscriber threshold.

- `r/liberal` is included. Doesn't match keywords because `public_description` is "Welcome to /r/Liberal!`. Meets subscriber threshold.